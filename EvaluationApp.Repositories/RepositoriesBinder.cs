using EvaluationApp.Domains;
using EvaluationApp.Domains.PrivateKeys;
using Microsoft.Extensions.DependencyInjection;

namespace EvaluationApp.Repositories
{
    public static class RepositoriesBinder
    {

        public static void BindRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IPrivateKeysRepository, PrivateKeysMockRepository>();
        }
        
    }
}