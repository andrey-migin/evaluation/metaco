﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using EvaluationApp.Domains.PrivateKeys;

namespace EvaluationApp.Repositories
{
    public class PrivateKeyPairMockEntity : IPrivateKeyPair
    {
        public PrivateKeyPairId Id { get; private init; }
        public RSAParameters PrivateKey { get; private init; }
        public RSAParameters PublicKey { get; private init;}

        public static PrivateKeyPairMockEntity Create(IPrivateKeyPair src)
        {
            return new ()
            {
                Id = src.Id,
                PrivateKey = src.PrivateKey,
                PublicKey = src.PublicKey
            };
        }
    }
    
    public class PrivateKeysMockRepository : IPrivateKeysRepository
    {

        private readonly Dictionary<string, PrivateKeyPairMockEntity> _dictionaryById = new();

        private readonly ReaderWriterLockSlim _lockSlim = new ();
        
        public ValueTask AddAsync(IPrivateKeyPair pair)
        {
            _lockSlim.EnterWriteLock();
            try
            {
                _dictionaryById.Add(pair.Id.Value, PrivateKeyPairMockEntity.Create(pair));
            }
            finally
            {
                _lockSlim.ExitWriteLock();
            }

            return new ValueTask();
        }

        public ValueTask<IPrivateKeyPair> TryGetAsync(in PrivateKeyPairId id)
        {
            _lockSlim.EnterReadLock();
            try
            {
                return _dictionaryById.TryGetValue(id.Value, out var result) ?
                    new ValueTask<IPrivateKeyPair>(result) 
                    : new ValueTask<IPrivateKeyPair>();

            }
            finally
            {
                _lockSlim.ExitReadLock();
            }
        }
    }
}