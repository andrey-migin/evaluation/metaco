using EvaluationApp.Domains.PrivateKeys;
using Microsoft.Extensions.DependencyInjection;

namespace EvaluationApp.Domains
{
    public static class DomainsBinder
    {
        public static void BindDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<MyCryptoService>();
        }
    }
}