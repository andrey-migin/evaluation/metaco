﻿using System;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace EvaluationApp.Domains.PrivateKeys
{


    public struct PrivateKeyPairId
    {
        public string Value { get; private init; }

        public static PrivateKeyPairId GenerateNew()
        {
            return new ()
            {
                Value = Guid.NewGuid().ToString("N")
            };
        }

        public static PrivateKeyPairId CreateFromId(string id)
        {
            return new ()
            {
                Value = id
            };
        }
    }

    public interface IPrivateKeyPair
    {
        
        PrivateKeyPairId Id { get; }
        RSAParameters PrivateKey { get; }
        
        RSAParameters PublicKey { get; }
    }
    
    public interface IPrivateKeysRepository
    {
        ValueTask AddAsync(IPrivateKeyPair pair);

        ValueTask<IPrivateKeyPair> TryGetAsync(in PrivateKeyPairId id);
    }
    
}