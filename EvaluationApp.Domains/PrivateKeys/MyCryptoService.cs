using System;
using System.Security.Cryptography;

namespace EvaluationApp.Domains.PrivateKeys
{
    public class MyCryptoService
    {
        internal class PrivateKeyPair : IPrivateKeyPair
        {
            public PrivateKeyPairId Id { get; internal set; }
   
            public RSAParameters PrivateKey { get; internal set; }
            public RSAParameters PublicKey { get; internal set; }
        }
        
        
        public IPrivateKeyPair GenerateNewPair()
        {
            using var rsa = new RSACryptoServiceProvider(2048) {PersistKeyInCsp = false};

            return new PrivateKeyPair
            {
                Id = PrivateKeyPairId.GenerateNew(),
                PublicKey = rsa.ExportParameters(false), 
                PrivateKey = rsa.ExportParameters(true)
            };
        }

        public byte[] SignData(byte[] hashOfDataToSign, IPrivateKeyPair privateKeyPair)
        {
            using var rsa = new RSACryptoServiceProvider(2048) {PersistKeyInCsp = false};
            
            rsa.ImportParameters(privateKeyPair.PrivateKey);
                 
            var rsaFormatter = new RSAPKCS1SignatureFormatter(rsa);                
            rsaFormatter.SetHashAlgorithm("SHA256");
 
            return rsaFormatter.CreateSignature(hashOfDataToSign);
        }

        public bool VerifyData(byte[] signedData, byte[] signature, IPrivateKeyPair privateKeyPair)
        {
            using var rsa = new RSACryptoServiceProvider(2048);
            rsa.ImportParameters(privateKeyPair.PublicKey);
 
            var rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
            rsaDeformatter.SetHashAlgorithm("SHA256");
 
            return rsaDeformatter.VerifySignature(signedData, signature);
        }
    }
}