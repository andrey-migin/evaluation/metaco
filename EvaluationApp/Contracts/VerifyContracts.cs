namespace EvaluationApp.Contracts
{
    public class VerifyResponse
    {
        public bool SignIsValid { get; set; }
    }
}