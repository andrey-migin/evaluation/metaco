using System;

namespace EvaluationApp.Contracts
{
    public class SignResponse
    {
        public string Base64Result { get; set; }
    }


    public static class SignRequestUtils
    {

        public static byte[] ConvertBase64OrNull(this string base64String)
        {
            try
            {
                return Convert.FromBase64String(base64String);

            }
            catch
            {
                return null;
            }
        }
        
    }
}