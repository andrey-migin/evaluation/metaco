using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using EvaluationApp.Contracts;
using EvaluationApp.Domains.PrivateKeys;
using Microsoft.AspNetCore.Mvc;

namespace EvaluationApp.Controllers
{
    [ApiController]
    public class HomeController : Controller
    {
        private readonly MyCryptoService _myCryptoService;
        private readonly IPrivateKeysRepository _privateKeysRepository;

        public HomeController(MyCryptoService myCryptoService, IPrivateKeysRepository privateKeysRepository)
        {
            _myCryptoService = myCryptoService;
            _privateKeysRepository = privateKeysRepository;
        }


        [HttpPost("/GenerateNewKeyPair")]
        public async ValueTask<GenerateResponse> GenerateNew()
        {
            var newPair = _myCryptoService.GenerateNewPair();

            await _privateKeysRepository.AddAsync(newPair);

            return new GenerateResponse
            {
                Id = newPair.Id.Value
            };
        }

        [HttpPost("/CalcSha256")]
        public string CalcSha256([Required] [FromForm] string someString)
        {
            var result = Encoding.UTF8.GetBytes(someString);
            using var sha256 = SHA256.Create();
            return Convert.ToBase64String(sha256.ComputeHash(result));
        }


        private (IActionResult errorResult, byte[] inputBytes) DecodeBase64Sign([Required] [FromForm] string base64DataToSign)
        {
            var data = base64DataToSign.ConvertBase64OrNull();
            
            if (data == null)
                return (BadRequest("Invalid base64 string"), null);
            
            if (data.Length != 32)
                return (BadRequest("Array sequence encoded by base64 must contain exactly 32 bytes"), null);


            return (null, data);
        }
        

        [HttpPost("/Sign")]
        public async ValueTask<IActionResult> Sign([Required][FromForm]string id, [Required][FromForm]string base64HashOfDoc)
        {
            var pairId = PrivateKeyPairId.CreateFromId(id);
            var keyPair = await _privateKeysRepository.TryGetAsync(pairId);

            if (keyPair == null)
                return NotFound($"Private Key is not found with ID: {id}");

            var (err, dataToSign) = DecodeBase64Sign(base64HashOfDoc);

            if (err != null)
                return err;
          
            var resultData = _myCryptoService.SignData(dataToSign, keyPair);

            var result = new SignResponse
            {
                Base64Result = Convert.ToBase64String(resultData)
            };

            return Json(result);
        }
        
        [HttpPost("/Verify")]
        public async ValueTask<IActionResult> Verify([Required][FromForm]string id,
            [Required][FromForm]string base64HashOfDoc, [Required][FromForm]string base64Signature)
        {
            var pairId = PrivateKeyPairId.CreateFromId(id);
            var keyPair = await _privateKeysRepository.TryGetAsync(pairId);

            if (keyPair == null)
                return NotFound($"Private Key is not found with ID: {id}");

            var (err, dataToSign) = DecodeBase64Sign(base64HashOfDoc);

            if (err != null)
                return err;

            var signature = base64Signature.ConvertBase64OrNull();
          
            var resultData = _myCryptoService.VerifyData(dataToSign, signature, keyPair);

            var result = new VerifyResponse
            {
                SignIsValid = resultData
            };

            return Json(result);
        }
        
    }
}